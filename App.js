import { createStackNavigator, createAppContainer } from 'react-navigation';
import Home from './pages/Home';
import PlaceDetail from './pages/PlaceDetail';
// Add the pages to redirect from bottom bar
import Chat from './pages/Chat';
import Map from './pages/Map';
import QrCode from './pages/QrCode';
// Add the pages to login admin
import LogIn from './pages/LogIn';
// Add the page to admin user
import Admin from './pages/Admin';
import ListSites from './pages/ListSites';
import NewPlace from './pages/NewPlace';
import CommentPlace from './pages/CommentPlace';

const MainNavigator = createStackNavigator({
  Home: { screen: Home },
  Place: { screen: PlaceDetail },
  Chat: { screen: Chat },
  Map: { screen: Map },
  Qr: { screen: QrCode },
  Login: { screen: LogIn },
  Admin: { screen: Admin },
  ListSites: { screen: ListSites },
  NewPlace: { screen: NewPlace },
  Comment: { screen: CommentPlace },
});

const App = createAppContainer(MainNavigator);
export default App;