import React, { Component } from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';

export default class BottomBar extends Component {
    constructor(props) {
        super(props);
    }

    goToChat = () => {
        //alert(JSON.stringify(place));
        this.props.navigation.navigate('Chat');
    }

    goToMap = () => {
        //alert(JSON.stringify(place));
        this.props.navigation.navigate('Map');
    }

    goToQr = () => {
        this.props.navigation.navigate('Qr');
    }

    render() {
        return (
            <View style={bottomBar.container}>
                <TouchableOpacity onPress={ () => this.goToChat() }>
                    <Image source={require('./images/ic_chat.png')} style={bottomBar.icon} />
                </TouchableOpacity>
                <TouchableOpacity onPress={ () => this.goToMap() }>
                    <Image source={require('./images/ic_map.png')} style={bottomBar.icon} />
                </TouchableOpacity>
                <TouchableOpacity onPress={ () => this.goToQr() }>
                    <Image source={require('./images/ic_qr.png')} style={bottomBar.icon} />
                </TouchableOpacity>
            </View>
        );
    }
}

const bottomBar = StyleSheet.create({
    container: {
      alignContent: 'center',
      backgroundColor: '#c7a4ff',
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-around',
      maxHeight: 50,
      padding: 10,
    },
    icon: {
      maxHeight: 30,
      maxWidth: 30,
    }
  });