import { Platform, StyleSheet } from 'react-native';

module.exports = {
  mainContent: StyleSheet.create({
    container: {
      backgroundColor: '#FFFFFF',
      flex: 2,
      padding: 15,
    }
  }),
  placeDetail: StyleSheet.create({
      slide: {
          flex: 1,
          alignItems: 'center',
          paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
          backgroundColor: '#FFF8E1'
      },
      title: {
          fontSize: 25,
          fontWeight: 'bold',
          marginTop: 10,
          marginBottom: 10
      },
      rowContainer: {
          alignItems: 'center',
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
      },
      icon: {
          marginRight: 10,
          maxHeight: 20,
          maxWidth: 20
      },
      textDetail: {
          fontSize: 18,
          fontWeight: 'bold',
      },
      titleRating: {
          fontSize: 50,
          marginRight: 10
      }
  }),
  appBar: StyleSheet.create({
      container: {
        flex: 1,
      },
      appBar: {
        backgroundColor: '#9575cd',
        flex: 1,
        maxHeight: 120,
        minHeight: 120,
        paddingLeft: 25,
        paddingRight: 25,
        zIndex: 10
      },
      mainAppBar: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 8,
        marginTop: 8,
      },
      imgAppBar: {
        maxHeight: 30,
        maxWidth: 30
      },
      titleAppBar: {
        fontSize: 20,
        color: '#ffffff',
        marginBottom: 5,
      },
      searchAppBar: {
        alignItems: 'center',
        backgroundColor: '#ffffff',
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        maxHeight: 40,
        paddingRight: 10,
        paddingLeft: 10,
      },
      searchInput: {
        flex: 1,
        height: 40,
      },
      searchIcon: {
        maxHeight: 20,
        maxWidth: 20,
        padding: 10,
      }
  }),
  card: StyleSheet.create({
      container: {
        backgroundColor: '#ffffff',
        flex: 1,
        margin: 10,
        maxHeight: 280
      },
      picture: {
        height: 150,
        width: 360
      },
      title: {
        color: '#000000',
        fontSize: 23,
        fontWeight: 'bold',
        marginTop: 10,
        paddingLeft: 10,
      },
      button: {
        alignSelf: 'flex-end',
        color: 'blue',
        fontSize: 18,
        height: 25,
        maxWidth: 150,
        marginTop: 10,
        paddingRight: 20,
        textAlign: 'center',
      }
  }),
  buttons: StyleSheet.create({
      simple: {
        alignSelf: 'flex-end',
        color: 'blue',
        fontSize: 18,
        height: 25,
        maxWidth: 150,
        marginTop: 10,
        paddingRight: 20,
        textAlign: 'center',
      },
      primary: {
        backgroundColor: '#0000FF',
        borderRadius: 10,
        color: '#ffffff',
        fontSize: 22,
        padding: 10,
        textAlign: 'center',
        width: 270
      },
      primarySmall: {
        backgroundColor: '#0000FF',
        borderRadius: 10,
        color: '#ffffff',
        margin: 10,
        padding: 8,
        textAlign: 'center',
        width: 130
      }
  }),
  common: StyleSheet.create({
    container: {
      backgroundColor: '#FFFFFF',
      flex: 1,
      padding: 15,
    },
    formInput: {
      borderColor: '#E9967A',
      borderRadius: 5,
      borderWidth: 1,
      fontSize: 17,
      padding: 5,
      textAlign: 'left',
    },
    label: {
      color: '#000000',
      fontSize: 17,
      marginTop: 5,
      marginBottom: 5,
    },
    textArea: {
      height: 80,
    },
    title: {
      color: '#000000',
      fontSize: 23,
      fontWeight: 'bold',
      marginTop: 10,
      marginBottom: 10,
    },
    labelSmall: {
      color: '#FFFFFF',
      fontSize: 12
    },
  }),
};