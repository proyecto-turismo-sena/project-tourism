Descargar el código del proyecto, ya se clonando el repositorio o descargando el código desde el repositorio.
Ingresar a la carpeta en donde se guardo el código usando una terminal de comando e instalar todas las dependencias usando el comando:

> npm install

o el comando:

> yarn install

Una vez instaladas todas las dependencias, abrir el archivo **Slideshow.js** ubicado en la ruta: *node_modules/react-native-slideshow* dentro de la carpeta del proyecto, en el archivo realizar el siguiente cambio:
1. Eliminar la importación de *PropTypes* del import de 'react'

   -import React. { Component, PropTypes } from 'react';
   +import React, { Component ] from 'react';

2. Agregar importación de *PropTypes* desde el paquete *prop-types*

    +import  PropTypes  from  'prop-types';
