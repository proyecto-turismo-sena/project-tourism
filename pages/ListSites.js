import React, { Component } from 'react';
import { Alert, FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Button, Icon, ListItem, SearchBar } from 'react-native-elements';
import { ProgressDialog } from 'react-native-simple-dialogs';
// importar estilos CSS de los elementos
import { common, buttons } from '../assets/styles/Styles';

export default class ListSites extends Component {
    constructor(props) {
        super(props);

        this.state = {
            search: '',
            ipAddress: 'https://alejosaag-tourismws.herokuapp.com/',
            pathPlace: 'places',
            list: [],
            progressVisible: true,
        };
    }

    /**
   * Ejecuta el metodo que consulta los sitios registrados mientras se carga el componente.
   */
    async componentWillMount() {
        this.getAllPlaces();
    }

    /**
     * Consulta los sitios registrados y activos
     */
    getAllPlaces = async () => {
        try {
            let auth = await fetch(`${this.state.ipAddress}${this.state.pathPlace}`, {
                method: 'GET',
            });
            let response = await auth.json();

            if (response.result) {
                let data = [];
                response.places.forEach((item) => {
                // Obtiene la imagen para mostrar.
                let image = `${this.state.ipAddress}image/image-not-found.png`;
                if (item.hasOwnProperty('picture1')) {
                    image = `${this.state.ipAddress}image/${item.picture1}`;
                }
                
                let place = {
                    key: item._id,
                    name: item.name,
                    subtitle: item.category.name,
                    avatar_url: image,
                };
                data.push(place);
                });

                this.setState({ list: data });
            } else {
                alert(response.err.message);
            }
        } catch (error) {
            alert('Se ha presentado un error, intenta mas tarde');
        }

        this.setState({ progressVisible: false });
    }

    /**
     * Realiza la consulta de sitios por el nombre
     */
    updateSearch = search => {
        this.setState({ search });
        this.searchPlaceByName();
        //alert(this.state.search);
    };

    /**
     * Busca sitios por el nombre.
     */
    async searchPlaceByName() {
        try {
        let name = this.state.search;
        //alert(`${this.state.ipAddress}${this.state.pathPlace}/${name}`);

        let auth = await fetch(`${this.state.ipAddress}${this.state.pathPlace}/${name}`, {
            method: 'GET',
        });
        let response = await auth.json();

        if (response.result) {
            this.processPlaces(response.places);
        } else {
            this.setState({ progressVisible: false });
            alert(response.err.message);
        }
        } catch (error) {
            this.setState({ progressVisible: false });
            alert('Se ha presentado un error, intenta mas tarde');
        }
    }

    /**
     * Elimina el sitio seleccionado
     */
    deletePlace = async (idPlace) => {
        try {
            let auth = await fetch(`${this.state.ipAddress}place/${idPlace}`, {
                method: 'DELETE',
            });
            let response = await auth.json();
            if (response.result) {
                this.getAllPlaces();
            } else {
                this.setState({ progressVisible: false });
                alert(response.err.message);
            }
        } catch (e) {
            alert('Se presento un error al eliminar el sitio');
        }
    }

    confirmDelete = (place) => {
        Alert.alert(
            'Eliminar',
            `¿Esta seguro desea eliminar el sition ${place['name']}?`,
            [
              {
                text: 'Cancelar',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'Aceptar', onPress: () => this.deletePlace(place['key'])},
            ],
            {cancelable: false},
          );
    }

    /**
     * Recorre el arreglo obtenido desde el servicio web y genera un arreglo para mostrar los sitios.
     */
    processPlaces = (places) => {
        
        let data = [];
        places.forEach((item) => {
        // Obtiene la imagen para mostrar.
        let image = `${this.state.ipAddress}image/image-not-found.png`;
        if (item.hasOwnProperty('picture1')) {
            image = `${this.state.ipAddress}image/${item.picture1}`;
        }
        
        let place = {
            key: item._id,
            name: item.name,
            subtitle: item.category.name,
            avatar_url: image,
        };
        data.push(place);
        });

        this.setState({ list: data, progressVisible: false });
        //alert(JSON.stringify(data));
    };

    keyExtractor = (item, index) => index.toString()

    renderItem = ({ item }) => (
        <ListItem
            title={item.name}
            subtitle={
                <View style={{flexDirection: 'column',paddingTop: 5}}>
                  <Text style={{color: 'grey'}}>{item.subtitle}</Text>
                  <TouchableOpacity
                    style={{ alignItems: 'center', backgroundColor: 'red', padding: 10, width: 100 }}
                    onPress={ () => this.confirmDelete(item) }
                    >
                        <Text style={{color: '#FFF'}}>Eliminar</Text>
                    </TouchableOpacity>
                </View>
              }
            leftAvatar={{ source: { uri: item.avatar_url } }}
            bottomDivider
        />
    )

    render() {
        const { search } = this.state;

        return (
            <View style={common.container}>
                <Text style={common.title}>Lista de sitios</Text>
                <SearchBar
                    placeholder="Ingrese aqui el nombre..."
                    onChangeText={this.updateSearch}
                    value={search}
                />
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.state.list}
                    renderItem={this.renderItem}
                    />
                <Button
                    title="Nuevo sitio"
                    type="solid"
                    raised={true}
                    onPress={ () => this.props.navigation.navigate('NewPlace') }
                    />
                <ProgressDialog
                    visible={this.state.progressVisible}
                    message="Por favor espera..."
                />
            </View>
        );
    }
}

styles = StyleSheet.create({
    subtitleView: {
      flexDirection: 'column',
      flex: 1,
      paddingTop: 5
    },
    ratingImage: {
      height: 19.21,
      width: 100
    },
    ratingText: {
      color: 'grey'
    }
  })