import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Button } from 'react-native-elements';
// importar estilos CSS de los elementos
import { common, buttons } from '../assets/styles/Styles';

export default class Admin extends Component {
    constructor(props) {
        super(props);
    }

    listSites = () => {
        //alert('listar sitios');
        this.props.navigation.navigate('ListSites');
    };

    render() {
        return (
            <View style={common.container}>
                <Text style={common.title}>Bienvenido</Text>
                <Button
                    title="Listar sitios"
                    type="outline"
                    raised={true}
                    onPress={ this.listSites }
                    />
            </View>
        );
    }
}