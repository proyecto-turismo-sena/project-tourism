import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';

export default class LogIn extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userName: '',
            password: '',
            errUserName: 'Debe ingresar el nombre del usuario',
            errPasswd: 'Debe ingresar la contraseña',
            showErrUser: false,
            showErrPass: false,
            ipAddress: '192.168.0.15',
            port: '3000'

        };
    }

    logIn = () => {
        // Se valida si se ha ingresado el nombre de usuario.
        let showErrUserName = (this.state.userName.trim() == "");
        this.setState({ showErrUser: showErrUserName });
        // Se valida que se hay ingresado la contraseña.
        let showErrPasswd = (this.state.password.trim() == "");
        this.setState({ showErrPass: showErrPasswd });

        this.props.navigation.navigate('Admin');

        if (!showErrUserName && !showErrPasswd)
            this.doLogin();
    }

    async doLogin() {
        try {
            let body = `email=${this.state.userName}&password=${this.state.password}`;
            let auth = await fetch(`http://${this.state.ipAddress}:${this.state.port}/login`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                body: body
            });
            let response = await auth.json();

            if (response.auth)
                this.props.navigation.navigate('Admin');
            else
                alert(response.err.message);
        } catch (error) {
            alert('Se ha presentado un error, intenta mas tarde');
        }
    }

    render() {
        return (
            <View style={SessionIn.container}>
                <TextInput 
                    style={SessionIn.formInput}
                    onChangeText={ (user) => this.setState({ userName: user }) }
                    placeholder="Correo electrónico" />
                <Text style={this.state.showErrUser ? SessionIn.errorLabel : SessionIn.hideElem}>{ this.state.errUserName }</Text>
                <TextInput 
                    style={SessionIn.formInput}
                    onChangeText={ (passwd) => this.setState({ password: passwd }) }
                    secureTextEntry={ true }
                    placeholder="Digite la contraseña" />
                <Text style={this.state.showErrPass ? SessionIn.errorLabel : SessionIn.hideElem}>{ this.state.errPasswd}</Text>
                <TouchableOpacity onPress={ this.logIn }>
                    <Text
                        style={SessionIn.btnPrimary}>
                            Ingresar
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const SessionIn = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    formInput: {
        borderColor: '#E9967A',
        borderRadius: 10,
        borderWidth: 1,
        fontSize: 22,
        marginBottom: 10,
        padding: 5,
        textAlign: 'center',
        width: 270
    },
    btnPrimary: {
        textAlign: 'center',
        backgroundColor: '#0000FF',
        color: '#ffffff',
        fontSize: 22,
        padding: 10,
        width: 270
    },
    errorLabel: {
        color: 'red',
        fontSize: 15,
        margin: 5
    },
    hideElem: {
        display: 'none'
    },
});