import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { ProgressDialog } from 'react-native-simple-dialogs';

export default class QrCode extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            ipAddress: 'https://alejosaag-tourismws.herokuapp.com/',
            pathPlace: 'places',
          places: [],
          progressVisible: false,
          placeName: '',
        };
    }

    /*componentDidMount = () => {
        this.checkData(JSON.parse("{\"id\": \"5d4f8cded24a8a001722aa70\", \"name\": \"Hotel%20Aristi\"}"));
    }*/

    checkData = async (qrCode) => {
        try {
            this.setState({ progressVisible: true });
            qrCode = JSON.parse(qrCode);
            let name = qrCode.name;
            //alert(`/${name}`);
      
            let auth = await fetch(`${this.state.ipAddress}${this.state.pathPlace}/${name}`, {
                method: 'GET',
            });
            let response = await auth.json();
      
            if (response.result) {
              this.setState({ progressVisible: false });
              this.props.navigation.navigate('Place', {
                place: JSON.stringify(response.places[0])
              });
              //alert(JSON.stringify(response));
            } else {
                this.setState({ progressVisible: false });
              alert(`No se encontro un sitio con el nombre: ${name}`);
            }
          } catch (error) {
                this.setState({ progressVisible: false });
                //alert(JSON.stringify(error));
              alert('Se ha presentado un error, intenta mas tarde');
          }
    };

    /*takePicture = async() => {
        if (this.camera) {
          const options = { quality: 0.5, base64: false };
          const data = await this.camera.takePictureAsync(options);
          alert(data[0].data);
          this.checkData(data[0].data);
        }
    };*/

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <Text style={{ alignSelf: 'center', margin: 10 }}>Escanear código QR</Text>
                <View style={styles.container}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={styles.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode.on}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    androidRecordAudioPermissionOptions={{
                        title: 'Permission to use audio recording',
                        message: 'We need your permission to use your audio',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    onGoogleVisionBarcodesDetected={({ barcodes }) => {
                        //alert(JSON.stringify(barcodes[0].data));
                        this.checkData(barcodes[0].data);
                    }}
                    />
                </View>

                <ProgressDialog
                    visible={this.state.progressVisible}
                    message="Por favor espera un momento..."
                    />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'black',
      justifyContent: 'center',
      alignItems: 'center',
    },
    preview: {
      flex: 1,
      maxHeight: 150,
      width: 150,
    },
    capture: {
      flex: 0,
      backgroundColor: '#fff',
      borderRadius: 5,
      padding: 15,
      paddingHorizontal: 20,
      alignSelf: 'center',
      margin: 20,
    },
});