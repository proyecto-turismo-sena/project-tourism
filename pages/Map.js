import React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import GetLocation from 'react-native-get-location';
import MapViewDirections from 'react-native-maps-directions';

import { mapApiKey } from '../app.json';

//Funcion para identificar los metros de distancia entre 2 puntos.
function getDistance(p1, p2) {
    rad = x => x * Math.PI / 180;
    var R = 6378137; //radio de la tierra en metros
    var dLat = rad(p2.latitude - p1.latitude);
    var dLong = rad(p2.longitude - p1.longitude);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(p1.latitude)) * Math.cos(rad(p2.latitude)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
}

export default class Map extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ipAddress: 'https://alejosaag-tourismws.herokuapp.com/',
            pathPlace: 'places',
            latitude: 0,
            longitude: 0,
            currLatitude: 0,
            currLongitude: 0,
            places: [],
            markers: [],
            name: '',
        }
    }

    /**
     * Obtiene las coordenadas actuales de ubicacion.
     */
    componentWillMount() {
        // Obtiene las coordenadas de la ubicacion actual.
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
        .then(location => {
            //alert(JSON.stringify(location));
            this.setState({ latitude: location.latitude, longitude: location.longitude });
            this.setState({ currLatitude: location.latitude, currLongitude: location.longitude });
        })
        .catch(error => {
            const { code, message } = error;
            console.warn(code, message);
        });

        this.getInfoPlaces();
    }

    /**
     * Consulta la informacion de los sitios consumiendo el servicio web.
     */
    async getInfoPlaces() {
        
        try {
            let auth = await fetch(`${this.state.ipAddress}${this.state.pathPlace}`, {
                method: 'GET'
            });
            let response = await auth.json();
      
            if (response.result) {
                //alert(JSON.stringify(response));
              this.setState({ places: response.places });
            } else {
              //this.setState({ progressVisible: false });
              alert(response.err.message);
            }
        } catch (error) {
            //this.setState({ progressVisible: false });
            //alert(JSON.stringify(error));
            //console.log(error);
        }
    }

    /**
     * Recorre los sitios y compara las coordenadas para determinar cuales estan cerca al punto.
     */
    getNearestPlaces = (LatLon) => {
        let data = [];
        let places = this.state.places;
        let counter = 1;
        
        places.forEach((place, idx) => {
            let coords = {
                latitude: parseFloat(place.latitude),
                longitude: parseFloat(place.longitude)
            };
            // Se invoca la funcion pasandole las coordenadas del punto actual y las del sitio.
            let dist = getDistance(LatLon, coords);

            // Si la distancia es menor a la definida para el limite se agrega el sitio.
            if (dist <= 500) {
                let mark = {
                    key: counter,
                    latlng: coords,
                    title: place.name,
                    description: place.name
                };

                data.push(mark);
                counter++;
            }
            
        });
        
        return data;
    };

    render() {
        // Se obtienen las coordenadas envaiadas por props.
        const { navigation } = this.props;
        const lati = parseFloat(navigation.getParam('latitude', this.state.latitude));
        const longi = parseFloat(navigation.getParam('longitude', this.state.longitude));
        
        //const destination = { latitude: lati, longitude: longi };
        //const origin = { latitude: this.state.currLatitude, longitude: this.state.currLongitude };

        const title = navigation.getParam('title', '');
        
        const LatLng = {
            latitude: lati,
            longitude: longi,
        };
        
        // Si las coordenadas no son 0, ejecuta la busqueda de cercania
        let markers = [];
        if (title === '') {
            markers = this.getNearestPlaces(LatLng);
        }
        
        //alert(JSON.stringify(navigation.getParam('latitude', this.state.latitude)));
        return (
            <View style={styles.container}>
                <MapView
                    style={styles.map}
                    region={{
                        latitude: lati,
                        longitude: longi,
                        latitudeDelta: 0.04,
                        longitudeDelta: 0.05,
                    }}
                    showsUserLocation={true}
                    zoomControlEnabled={true}
                    >
                        <Marker
                            title={title}
                            description={title}
                            coordinate={LatLng}
                        />
                        {markers.map(marker => (
                            <Marker
                                coordinate={marker.latlng}
                                title={marker.title}
                                description={marker.title}
                                />
                        ))}
                        { title === '' ?
                            (<MapView.Circle
                                center = {LatLng}
                                radius = {500}
                            />)
                        : null }
                </MapView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
});