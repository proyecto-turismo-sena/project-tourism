/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Button, Platform, Image, FlatList, StyleSheet, Text, TouchableOpacity, TextInput, View } from 'react-native';
import { Rating } from 'react-native-ratings';
import { ProgressDialog } from 'react-native-simple-dialogs';
import AsyncStorage from '@react-native-community/async-storage';
// Importar componente de la barra de navegación inferior
import BottomBar from '../components/bottomBar/bottomBar';
// importar estilos CSS de los elementos
import {appBar, card} from '../assets/styles/Styles';

import Notifications from '../components/notifications/Notifications';

type Props = {};
export default class Home extends Component<Props> {

  constructor(props) {
    super(props);

    this.state = {
      ipAddress: 'https://alejosaag-tourismws.herokuapp.com/',
      pathPlace: 'places',
      places: [],
      progressVisible: true,
      placeName: '',
    };
  }

  /**
   * Consulta los sitios registrados mientras se carga el componente.
   */
  async componentWillMount() {

    try {
      let auth = await fetch(`${this.state.ipAddress}${this.state.pathPlace}`, {
          method: 'GET'
      });
      let response = await auth.json();

      //alert(JSON.stringify(response.places)); 
      if (response.result) {
        this.setState({ progressVisible: false });
        this.processPlaces(response.places);
        this.countPlace();
        this.storePlaces(response.places);
      } else {
        this.setState({ progressVisible: false });
        alert(response.err.message);
      }
    } catch (error) {
      this.setState({ progressVisible: false });
      //alert('Se ha presentado un error, intenta mas tarde');
      this.getData();
    }
  }

  /**
   * Busca sitios por el nombre.
   */
  async searchPlaceByName() {
    try {
      let name = this.state.placeName;

      let auth = await fetch(`${this.state.ipAddress}${this.state.pathPlace}/${name}`, {
          method: 'GET',
      });
      let response = await auth.json();

      if (response.result) {
        this.processPlaces(response.places);
      } else {
        alert(`No se encontro un sitio con el nombre: ${name}`);
      }
    } catch (error) {
        alert('Se ha presentado un error, intenta mas tarde');
    }
  }

  /**
   * Recorre el arreglo obtenido desde el servicio web y genera un arreglo para mostrar los sitios.
   */
  processPlaces = (places) => {
    let data = [];
    
    places.forEach((item) => {
      // Obtiene la imagen para mostrar.
      let image = `${this.state.ipAddress}image/image-not-found.png`;
      /*let image1 = `${this.state.ipAddress}image/image-not-found.png`;
      let image2 = `${this.state.ipAddress}image/image-not-found.png`;*/
      if (item.hasOwnProperty('picture1')) {
        image = `${this.state.ipAddress}image/${item.picture1}`;
      }
      /*if (item.hasOwnProperty('picture2')) {
        image = `${this.state.ipAddress}image/${item.picture2}`;
      }
      if (item.hasOwnProperty('picture3')) {
        image = `${this.state.ipAddress}image/${item.picture3}`;
      }*/
      
      let place = {
        key: item._id,
        name: item.name,
        address: item.address,
        email: item.email,
        website: item.website,
        phone: item.phone,
        picture: image,
        //picture1: image1,
        //picture2: image2
        latitude: item.latitude,
        longitude: item.longitude,
        fav: require('../assets/images/ic_heart.png'),
        rating: item.rating
      };
      data.push(place);
    });

    this.setState({ places: data, progressVisible: false });
    //alert(JSON.stringify(data));
    //this.getFavourites();
  };

  /**
   * Guarda la informacion de los sitios turisticos en la base de datos local.
   * 
   * Guarda la informaciob de los sitios turisticos obtenidos desde el servicio web en
   * una base de datos de preferencias para consultarlos cuando no se detecte conexion a internet.
   */
  storePlaces = async (places) => {
    try {
      await AsyncStorage.setItem('@places', JSON.stringify(places))
    } catch (e) {
      // saving error
    }
  }

  countPlace = async () => {
    try {
      let numPlaces = this.state.places.length;
      let prevPlaces = 0;
      const value = await AsyncStorage.getItem('@places')
      if(value !== null) {
        // value previously stored
        //alert(value);
        prevPlaces = (JSON.parse(value)).length;
      }

      //alert(`web: ${numPlaces} - reg: ${prevPlaces}`);
      if (numPlaces != prevPlaces) {
        Notifications.showNotification('Sitios actualizados', 'Se han agregado/eliminados sitios');
      }
    } catch (e) {
      // some error
    }
  }

  /**
   * Consulta la informacion de los sitios turisticos almacenados.
   * 
   * Obtiene la informacion de los sitios turisticos que se ha almacenado en las preferencias
   * de la aplicacion.
   */
  getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@places')
      if(value !== null) {
        // value previously stored
        //alert(value);
        this.processPlaces(JSON.parse(value));
      }
    } catch(e) {
      // error reading value
    }
  }

  /**
   * Método para direccionar a la página del detalle del sitio.
   * 
   * place: objecto del sitio.
   */
  goToPlace = (place) => {
    //alert(JSON.stringify(place));
    this.props.navigation.navigate('Place', {
      place: JSON.stringify(place)
    });
  }

  /**
   * Agrega o elimina un sitio del listado de sitios marcados como favoritos
   */
  addPlaceToFavourites = async (idPlace) => {
    try {
      //await AsyncStorage.removeItem('@favs')
      const value = await AsyncStorage.getItem('@favs');
      let favs = [];

      if (value != null) {
        favs = JSON.parse(value);
        favs.push(idPlace);
      } else {
        favs = [ idPlace ];
      }
      //alert(favs);
      //await AsyncStorage.removeItem('@favs')
      await AsyncStorage.setItem('@favs', JSON.stringify(favs));
    } catch(error) {
      alert(error);
    }
  }

  /**
   * Obtiene los sitios que han sido marcado como favoritos y cambia la imagen
   */
  getFavourites = async () => {
    let favs = [];
    const places = this.state.places;
    
    try {
      const value = await AsyncStorage.getItem('@favs')
      if(value !== null) {
        favs = JSON.parse(value);
      }
    } catch(e) {
      // error reading value
    }

    places.forEach((place, idx) => {
      //alert(place.key);
      for (let i = 0; i < favs.length; i++) {
        if (place.key == favs[i]) {
          //alert(favs[i]);
          place.fav = require('../assets/images/ic_heart_filled.png');
        }/* else {
          alert('no esta');
        }*/
      }
    });
  }

  render() {
    this.getFavourites();

    return (
      <View style={appBar.container}>
        <View style={appBar.appBar}>
          <View style={appBar.mainAppBar}>
            <Text style={appBar.titleAppBar}>Insert name here!</Text>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
              <Image style={appBar.imgAppBar} source={require("../assets/images/ic_start_session.png")} />
            </TouchableOpacity>
          </View>
          <View style={appBar.searchAppBar}>
            <TextInput
              style={appBar.searchInput}
              onChangeText={ (name) => this.setState({ placeName: name }) }
              placeholder="Buscar un sitio" />
            <TouchableOpacity onPress={() => this.searchPlaceByName()}>
              <Image style={appBar.searchIcon} source={require('../assets/images/ic_search.png')} />
            </TouchableOpacity>
          </View>
        </View>

        <View style={mainContent.container}>
          <FlatList
            data={this.state.places}
            renderItem={({ item }) => <View style={card.container}>
              <Image source={{uri: item.picture}} style={card.picture} />

              <View style={{alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={card.title}>{item.name}</Text>
                <TouchableOpacity onPress={ () => this.addPlaceToFavourites(item.key) }>
                  <Image style={{ marginRight: 10 }} source={item.fav} />
                </TouchableOpacity>
              </View>

              <Rating
                imageSize={17}
                readonly
                ratingColor='#3498db'
                ratingBackgroundColor='#c8c7c8'
                ratingCount={5}
                onFinishRating={this.ratingCompleted}
                startingValue={item.rating}
                style={{ alignSelf: 'flex-start', marginLeft: 10, paddingVertical: 10 }}
              />
              <TouchableOpacity onPress={() => this.goToPlace(item)}><Text style={card.button}>Ver más</Text></TouchableOpacity>
            </View>}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>

        <ProgressDialog
          visible={this.state.progressVisible}
          message="Por favor espera un momento..."
        />

        <BottomBar navigation={this.props.navigation} />
      </View>
    );
  }
}

const mainContent = StyleSheet.create({
  container: {
    backgroundColor: '#e0e0e0',
    padding: 15,
    flex: 2
  }
});