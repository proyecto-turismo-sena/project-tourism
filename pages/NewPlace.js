import React from 'react';
import { Alert, Image, Picker, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import GetLocation from 'react-native-get-location';
import { Button } from 'react-native-elements';
import Modal from "react-native-modal";
import { RNCamera } from 'react-native-camera';
//var FormData = require('form-data');

// importar estilos CSS de los elementos
import { common, placeDetail } from '../assets/styles/Styles';
import { ScrollView } from 'react-native-gesture-handler';

export default class NewPlace extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            address: '',
            web_site: '',
            email: '',
            phone: '',
            mobile_phone: '',
            category_id: '',
            latitude: '',
            longitude: '',
            picture1: '',
            picture2: '',
            picture3: '',
            ipAddress: 'https://alejosaag-tourismws.herokuapp.com/',
            pathCategory: 'category',
            pathPlace: 'place',
            listCategories: [],
            isModalVisible: false,
            numPicture: 0,
            backCamera: true,
        }
    }

    // Consulta las categrias de sitios mientras se carga el componente.
    async componentWillMount() {
        try {
            let auth = await fetch(`${this.state.ipAddress}${this.state.pathCategory}`, {
                method: 'GET',
            });
            let response = await auth.json();

            if (response.result)
                this.setState({ listCategories: response.categories });
            else
                alert(response.err.message);
        } catch (error) {
            alert('Se ha presentado un error, intenta mas tarde');
        }
    }

    // Guardar el nuevo sitio.
    savePlace = () => {
        // alert('a guardar');
        this.addNewPlace();
    };

    /**
     * Consume el servicio para registrar un nuevo sitio.
     */
    async addNewPlace() {
        try {
            const data = new FormData();
            data.append('name', this.state.name);
            data.append('address', this.state.address);
            data.append('website', this.state.web_site);
            data.append('email', this.state.email);
            data.append('mobile_phone', this.state.mobile_phone);
            data.append('phone', this.state.phone);
            data.append('category_id', this.state.name);
            data.append('category', this.state.category_id);
            data.append('latitude', this.state.latitude);
            data.append('longitude', this.state.longitude);

            if (this.state.picture1.trim().length > 0) {
                let rpcName = this.state.picture1.replace(' ', '_');
                let splitName = this.state.picture1.split('.').reverse();
                let typePict = `image/${splitName[0]}`;
                let namePict = `${rpcName}.${splitName[0]}`;
                data.append('image1', {
                    uri: this.state.picture1,
                    type: typePict,
                    name: namePict
                });
            }
            if (this.state.picture2.trim().length > 0) {
                let rpcName = this.state.picture2.replace(' ', '_');
                let splitName = this.state.picture2.split('.').reverse();
                let typePict = `image/${splitName[0]}`;
                let namePict = `${rpcName}.${splitName[0]}`;
                data.append('image1', {
                    uri: this.state.picture2,
                    type: typePict,
                    name: namePict
                });
            }
            if (this.state.picture3.trim().length > 0) {
                let rpcName = this.state.picture3.replace(' ', '_');
                let splitName = this.state.picture3.split('.').reverse();
                let typePict = `image/${splitName[0]}`;
                let namePict = `${rpcName}.${splitName[0]}`;
                data.append('image1', {
                    uri: this.state.picture3,
                    type: typePict,
                    name: namePict
                });
            }

            let auth = await fetch(`${this.state.ipAddress}${this.state.pathPlace}`, {
                method: 'POST',
                headers: {
                    "Content-Type": "multipart/form-data"
                },
                body: data
            });
            let response = await auth.json();

            alert(JSON.stringify(response));
            if (response.result) {
                Alert.alert(
                    'Nuevo sitio registrado',
                    `Se ha registrado ${this.state.name} correctamente`,
                    [
                        {text: 'Aceptar', onPress: () => this.props.navigation.navigate('ListSites') }
                    ],
                    {cancelable: false},
                );
            } else {
                alert(response.err.message);
            }
        } catch (error) {
            alert('Se ha presentado un error, intenta mas tarde');
        }
    }

    /**
     * Obtiene las coordenadas de la posicon actual.
     */
    getLocation = () => {
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
        .then(location => {
            //alert(JSON.stringify(location));
            this.setState({ latitude: location.latitude, longitude: location.longitude });
        })
        .catch(error => {
            const { code, message } = error;
            console.warn(code, message);
        })
    };

    /**
     * Abre la ventana modal para la captura de la foto, establece que numero de foto se tomara.
     */
    openPictureModal = (numPicture) => {
        this.setState({ isModalVisible: true, numPicture });
    }

    /**
     * Cambia el estado de la ventana modal, de oculto a visible y viceversa
     */
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    /**
     * Cambia la camara del dispositivo, verdadero usa la camara trasera, falso usa la camara delantera
     */
    toggleCamera = () => {
        this.setState({ backCamera: !this.state.backCamera });
    };

    /**
     * Toma la foto y ubica el nombre en la caja correspondiente.
     */
    takePicture = async() => {
        if (this.camera) {
          const options = { quality: 0.5, base64: false };
          const data = await this.camera.takePictureAsync(options);
          //alert(data.uri);
          let numField = this.state.numPicture;

          if (numField === 1) {
            this.setState({ picture1: data.uri, isModalVisible: false });
          } else if (numField === 2) {
            this.setState({ picture2: data.uri, isModalVisible: false });
          } else if (numField === 3) {
            this.setState({ picture3: data.uri, isModalVisible: false });
          }
        }
    };

    render() {
        return (
            <ScrollView>
                <View style={common.container}>
                    <Text style={common.title}>Nuevo sitio</Text>
                    <Text style={common.label}>Nombre</Text>
                    <TextInput 
                        style={common.formInput}
                        onChangeText={ (name) => this.setState({ name: name }) }
                        placeholder="Nombre del sitio" />
                    <Text style={common.label}>Dirección</Text>
                    <TextInput 
                        style={common.formInput}
                        onChangeText={ (address) => this.setState({ address: address }) }
                        placeholder="Dirección del sitio" />
                    <Text style={common.label}>Sitio web</Text>
                    <TextInput 
                        style={common.formInput}
                        onChangeText={ (web_site) => this.setState({ web_site: web_site }) }
                        placeholder="Dirección del sitio web" />
                    <Text style={common.label}>Correo electronico</Text>
                    <TextInput 
                        style={common.formInput}
                        onChangeText={ (email) => this.setState({ email: email }) }
                        placeholder="Correo electronico" />
                    <Text style={common.label}>Telefono</Text>
                    <TextInput 
                        style={common.formInput}
                        onChangeText={ (phone) => this.setState({ phone: phone }) }
                        placeholder="Teléfono del sitio" />
                    <Text style={common.label}>Celular</Text>
                    <TextInput 
                        style={common.formInput}
                        onChangeText={ (mobile_phone) => this.setState({ mobile_phone: mobile_phone }) }
                        placeholder="Celular del sitio" />
                        
                    <Text style={common.label}>Categoria</Text>
                    <Picker
                        selectedValue={this.state.category_id}
                        onValueChange={(itemValue, itemIndex) =>
                            this.setState({category_id: itemValue})
                        }>
                        {
                            this.state.listCategories.map((item, key) => {
                                return <Picker.Item label={item.name} value={item._id} key={item._id} />
                            })
                        }
                    </Picker>

                    <Text style={common.label}>Capturar imagen 1</Text>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <TextInput 
                            style={common.formInput}
                            placeholder="Fotografia"
                            value={this.state.picture1} 
                            />
                        <TouchableOpacity onPress={ () => this.openPictureModal(1) }>
                            <Image source={require('../assets/images/camera_black.png')} style={placeDetail.icon} />
                        </TouchableOpacity>
                    </View>
                    <Text style={common.label}>Capturar imagen 2</Text>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <TextInput 
                            style={common.formInput}
                            placeholder="Fotografia"
                            value={this.state.picture2} 
                            />
                        <TouchableOpacity onPress={ () => this.openPictureModal(2) }>
                            <Image source={require('../assets/images/camera_black.png')} style={placeDetail.icon} />
                        </TouchableOpacity>
                    </View>
                    <Text style={common.label}>Capturar imagen 3</Text>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <TextInput 
                            style={common.formInput}
                            placeholder="Fotografia"
                            value={this.state.picture3} 
                            />
                        <TouchableOpacity onPress={ () => this.openPictureModal(3) }>
                            <Image source={require('../assets/images/camera_black.png')} style={placeDetail.icon} />
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity onPress={() => this.getLocation()} style={{ flex: 1, flexDirection: 'row'}} >
                        <Text style={placeDetail.textDetail}>Obtener coordenadas</Text>
                        <Image source={require('../assets/images/ic_position.png')} style={placeDetail.icon} />
                    </TouchableOpacity>

                    <Text style={common.label}>Latitud</Text>
                    <TextInput 
                        style={common.formInput}
                        onChangeText={ (latitude) => this.setState({ latitude: latitude }) }
                        value={this.state.latitude.toString()}
                        placeholder="Latitud del sitio" />

                    <Text style={common.label}>Longitud</Text>
                    <TextInput 
                        style={common.formInput}
                        onChangeText={ (longitude) => this.setState({ longitude: longitude }) }
                        value={this.state.longitude.toString()}
                        placeholder="Longitud del sitio" />

                    <Button
                        title="Guardar sitio"
                        type="solid"
                        raised={true}
                        onPress={ this.savePlace }
                        />

                    <Modal isVisible={this.state.isModalVisible}
                        backdropColor="#FFFFFF"
                        onBackdropPress={ this.toggleModal }>
                        <View style={{ flex: 1 }}>
                            <RNCamera
                                ref={ref => {
                                    this.camera = ref;
                                }}
                                style={styles.preview}
                                type={ this.state.backCamera ? 'back' : 'front' }
                                flashMode={RNCamera.Constants.FlashMode.on}
                                androidCameraPermissionOptions={{
                                    title: 'Permission to use camera',
                                    message: 'We need your permission to use your camera',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                                androidRecordAudioPermissionOptions={{
                                    title: 'Permission to use audio recording',
                                    message: 'We need your permission to use your audio',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                                onGoogleVisionBarcodesDetected={({ barcodes }) => {
                                    console.log(barcodes);
                                }}
                            />
                            <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={this.toggleCamera} style={styles.capture}>
                                    <Image source={this.state.backCamera ? require('../assets/images/camera_front_black.png') : require('../assets/images/camera_rear_black.png')} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                                    <Image source={require('../assets/images/round_camera_black.png')} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
});