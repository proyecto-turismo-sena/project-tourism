import React, { Component } from 'react';
import { Image, FlatList, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import { Button, ListItem } from 'react-native-elements';
import { ProgressDialog } from 'react-native-simple-dialogs';
// importar estilos CSS de los elementos
import { common, buttons, placeDetail } from '../assets/styles/Styles';
import { AirbnbRating } from 'react-native-ratings';

export default class CommentPlace extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: '',
            list: [],
            rating: 0,
            comment: '',
            ipAddress: 'https://alejosaag-tourismws.herokuapp.com/',
            pathComment: 'comment',
            progressVisible: false,
        };
    }

    /**
     * Consulta las observaciones realizadas al sitio.
     */
    componentWillMount = async () => {
        let id = this.props.navigation.getParam('idx', '');
        try {
            this.setState({ progressVisible: true });

            let comments = await fetch(`${this.state.ipAddress}${this.state.pathComment}/${id}`, {
                method: 'GET'
            });
            let response = await comments.json();

            this.setState({ progressVisible: false });
            if (response.result) {
                this.setState({ list: response.comments });
            } else {
                alert(response.err.message);
            }
        } catch (error) {
            this.setState({ progressVisible: false });
            alert('Se ha presentado un error, intenta mas tarde');
        }
    }

    keyExtractor = (item, index) => index.toString()

    renderItem = ({ item }) => (
        <ListItem
            title={item.comment}
            subtitle={
                <View style={styles.subtitleView}>
                  <Image source={require('../assets/images/ic_star_filled.png')} style={styles.ratingImage}/>
                  <Text style={styles.ratingText}>{item.rating}</Text>
                </View>
              }
        />
    )

    ratingCompleted( rating ) {
        alert( `Rating is: ${rating}` );
    }

    /**
     * Consume el servicio para registrar un nuevo comentario
     */
    makeComment = async (id) => {
        let { comment, rating } = this.state;
        
        try {
            this.setState({ progressVisible: true });

            const data = new FormData();
            data.append('comment', comment);
            data.append('rating', rating);
            data.append('place', id);

            let jsonData = `comment=${comment}&rating=${rating}&place=${id}`;

            let newComm = await fetch(`${this.state.ipAddress}${this.state.pathComment}`, {
                method: 'POST',
                headers: {'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'},
                body: jsonData
            });

            let response = await newComm.json();

            if (response.result) {
                let comments = this.state.list;
                comments.unshift(response.comment);
                this.setState({ list: comments });

                alert('Se ha registrado el nuevo comentario');
            } else {
                alert(response.err.message);
            }

            this.setState({ progressVisible: false });
        } catch (error) {
            this.setState({ progressVisible: false });
            alert('Se ha presentado un error, intenta mas tarde');
        }

        //alert(`${id} - ${comment} - ${rating}`);
    }

    render() {
        const { navigation } = this.props;
        const title = navigation.getParam('title', '');
        const id = navigation.getParam('idx', '');

        return (
            <View style={common.container}>
                <Text style={placeDetail.title}>{title}</Text>
                <AirbnbRating
                    showRating
                    selectedColor='#f1c40f'
                    reviews={['Terrible', 'Mal', 'Bien', 'Bueno', 'Genial']}
                    count={5}
                    size={25}
                    onFinishRating={(rat = this.ratingCompleted) => this.setState({ rating: rat })}
                    style={{ paddingVertical: 10 }}
                />
                <TextInput
                    multiline={true}
                    onChangeText={(text) => this.setState({ comment: text })}
                    style={[ common.formInput, common.textArea ]}
                    value={ this.state.comment }
                />
                <View style={{ alignContent: 'center' }}>
                    <Button
                        onPress={() => this.makeComment(id)}
                        title="Enviar comentario"
                        />
                </View>
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.state.list}
                    renderItem={this.renderItem}
                    />

                <ProgressDialog
                    visible={this.state.progressVisible}
                    message="Por favor espera un momento..."
                />
            </View>
        );
    }
}

styles = StyleSheet.create({
    subtitleView: {
      flexDirection: 'row',
      paddingLeft: 10,
      paddingTop: 5
    },
    ratingImage: {
      height: 19.21,
      width: 20
    },
    ratingText: {
      paddingLeft: 10,
      color: 'grey'
    }
  })