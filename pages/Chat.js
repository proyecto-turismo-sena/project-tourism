import React, { Component } from 'react';
import { FlatList, Text, TextInput, TouchableOpacity, View } from 'react-native';

export default class Chat extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            placeholder: 'Ingresar nombre de usuario',
            msg: '',
            messages: [],
        }

        this.socket = new WebSocket('ws://alejosaag-websocket.herokuapp.com');
        //this.socket = new WebSocket('ws://192.168.56.1:1337');

        this.socket.onmessage = (e) => {
            // a message was received
            const mess = JSON.parse(e.data);
            let mensajes = this.state.messages;
            //console.warn(mess);
            //console.warn(typeof mess);

            if (mess.type === "history") {
                mensajes.push(mess.data[0]);
                this.setState({ messages: mensajes });
            } else if (mess.type === "message") {
                mensajes.push(mess.data);
                this.setState({ messages: mensajes });
            }

            //console.warn(mensajes);
          };
    }

    componentDidMount() {
        this.socket.open = () => {
            this.socket.send('desde chat');
        }
    }

    sendMessage = () => {
        let user = this.state.username;
        let message = this.state.msg;

        if (user === '') {
            this.setState({ placeholder: 'Ingrese el mensaje', username: message });
        }

        this.socket.send(message);
        this.setState({ msg: '' });
    }

    _renderItem = ({item}) => (
        <View style={{alignItems: item.author === this.state.username ? 'flex-end' : 'flex-start'}}>
            <Text>{item.text}</Text>
            <Text style={{color: item.color}}>{item.author}</Text>
        </View>
    );

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column', margin: 10 }}>
                <FlatList style={{borderColor: '#CCC', borderRadius: 10, borderWidth: 2, flex: 1, flexDirection: 'column-reverse', padding: 10}}
                    data={this.state.messages}
                    renderItem={this._renderItem} />
                
                <View style={{ alignItems: 'stretch', borderColor: '#ccc', borderWidth: 1, flex: 1, flexDirection: 'row', maxHeight: 50, margin: 10 }}>
                    <TextInput 
                        style={{flex:1, padding: 5}}
                        placeholder={this.state.placeholder}
                        value={this.state.msg}
                        onChangeText={text => this.setState({ msg: text})} />
                    <TouchableOpacity style={{justifyContent: 'center', backgroundColor: '#9575cd', maxWidth: 60, padding: 5}} onPress={() => this.sendMessage()}>
                        <Text style={{color: '#fff', fontWeight: 'bold'}}>Enviar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}