import React, { Component } from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Slideshow from 'react-native-slideshow';
import { Rating } from 'react-native-ratings';

// importar estilos CSS de los elementos
import {buttons, common, mainContent, placeDetail} from '../assets/styles/Styles';
// Importar componente de la barra de navegación inferior
import BottomBar from '../components/bottomBar/bottomBar';

export default class PlaceDetail extends Component {
    constructor(props) {
        super(props);

        this.state= {
            position: 1,
            interval: null,
            dataSource: [
                {
                    url: require('../assets/images/cerro-cruces.jpeg')
                },
                {
                    url: require('../assets/images/cerro-cruces-1.jpg')
                },
                {
                    url: require('../assets/images/cerro-cruces-2.jpg')
                }
            ]
        };
    }

    componentWillMount() {
        // Establece el intervalo para cambiar la imagen.
        this.setState({
            interval: setInterval(() => {
                this.setState({
                    position: this.state.position === this.state.dataSource ? 0 : this.state.position + 1
                });
            }, 5000)
        });
    }

    componentWillUnmount() {
        clearInterval(this.state.interval);
    }

    /**
     * Redirecciona a la pagina del mapa para ubicar el sitio.
     */
    showInMap = (place) => {
        //alert(`lat: ${latitude} - lng: ${longitude}`);
        this.props.navigation.navigate('Map', {
            title: place.name,
            latitude: place.latitude,
            longitude: place.longitude
        });
    }

    /**
     * Redirecciona a la pagina de comentarios para consultar y hacer un nuevo comentario del sitio.
     */
    makeComment = (place) => {
        this.props.navigation.navigate('Comment', {
            title: place.name,
            idx: place.key
        });
    }

    render() {
        const { navigation } = this.props;
        const place = JSON.parse(navigation.getParam('place', 'no name'));
        //alert(JSON.stringify(place));
        const pictures = [];
        if (place.hasOwnProperty('picture')) {
            pictures.push({url: place.picture})
        } else if (place.hasOwnProperty('picture1')) {
            pictures.push({url: place.picture1})
        } else if (place.hasOwnProperty('picture2')) {
            pictures.push({url: place.picture2})
        }

        return (
            <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={place.slide}>
                    <Slideshow
                        dataSource={pictures}
                        position={this.state.position}
                        onPositionChanged={position => this.setState({ position })}
                    />
                </View>

                <View style={mainContent.container}>
                    <Text style={placeDetail.title}>{place.name}</Text>

                    <View style={placeDetail.rowContainer}>
                        <Text style={placeDetail.titleRating}>3.5</Text>
                        <Rating
                            imageSize={37}
                            readonly
                            ratingColor='#3498db'
                            ratingBackgroundColor='#c8c7c8'
                            ratingCount={5}
                            startingValue={place.rating}
                            onFinishRating={this.ratingCompleted}
                            style={{ paddingVertical: 10 }}
                        />
                    </View>
                    <View style={placeDetail.rowContainer}>
                        <TouchableOpacity style={buttons.primarySmall} onPress={ () => this.makeComment(place) }>
                            <Text style={common.labelSmall}>Calificar / Comentar</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={placeDetail.rowContainer}>
                        <Image source={require('../assets/images/ic_website.png')} style={placeDetail.icon} />
                        <Text style={placeDetail.textDetail}>{place.website}</Text>
                    </View>
                    <View style={placeDetail.rowContainer}>
                        <Image source={require('../assets/images/ic_place.png')} style={placeDetail.icon} />
                        <Text style={placeDetail.textDetail}>{place.address}</Text>
                    </View>
                    <View style={placeDetail.rowContainer}>
                        <Image source={require('../assets/images/ic_email.png')} style={placeDetail.icon} />
                        <Text style={placeDetail.textDetail}>{place.email}</Text>
                    </View>
                    <View style={placeDetail.rowContainer}>
                        <Image source={require('../assets/images/ic_phone.png')} style={placeDetail.icon} />
                        <Text style={placeDetail.textDetail}>{place.phone}</Text>
                    </View>
                    <View style={placeDetail.rowContainer}>
                        <Image source={require('../assets/images/ic_location.png')} style={placeDetail.icon} />
                        <TouchableOpacity onPress={ () => this.showInMap(place) }>
                            <Text style={buttons.simple}>Ver en mapa</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <BottomBar navigation={this.props.navigation} />
            </View>
        );
    }
}

const place = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
        backgroundColor: '#FFF8E1'
    }
});